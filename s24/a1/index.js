/*
Activity:
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of "Add s24 activity code".
16. Add the link in Boodle.
*/

/*
	// Template Literals
	4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
	5. Create a variable address with a value of an array containing details of an address.
	6. Destructure the array and print out a message with the full address using Template Literals.
*/
let numBase = 2;
let getCube = numBase ** 3;
console.log(`The cube of ${numBase} is ${getCube}`);

let address = [
	"2214",
	"Advincula Street",
	"Pasay City",
	"Metro Manila",
	"1305"
];

let [houseNumber, street, city, province, postalCode] = address;

console.log(`I live at ${houseNumber} ${street}, ${city}, ${province} ${postalCode}`);

/*
	// Object Destructuring
	7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
	8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

let animal = {
	animalName: "Lolong",
	animalDescription: "saltwater crocodile",
	weightInKg: 1075,
	sizeInFeet: 20,
	sizeInInches: 3
}

let {animalName, animalDescription, weightInKg, sizeInFeet, sizeInInches} = animal;

console.log(`${animalName} is a ${animalDescription}. He weighed at ${weightInKg} kgs with a measurement of ${sizeInFeet} ft ${sizeInInches} in.`);

/*
	// Arrow Functions
	9. Create an array of numbers.
*/

let numArray = [
	1,
	2,
	3,
	4,
	5
];

/*
	// A
	10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/

numArray.forEach((number)=>{
	console.log(`${number}`)
});

/*
	// B
	11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
*/

let reducedNumber = numArray.reduce((numA, numB)=>{
	return numA + numB;
});
console.log(reducedNumber);

/*
	// Javascript Objects
	12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
	13. Create/instantiate a new object from the class Dog and console log the object.
*/

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog();

myDog.name = "Lemon";
myDog.age = 7;
myDog.breed = "lemon beagle";
console.log(myDog);

let myNewDog = new Dog("Cookies&Cream", 10, "Dalmatian");
console.log(myNewDog);